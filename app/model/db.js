'user strict';

var mysql = require('mysql');

//local mysql db connection
var connection = mysql.createConnection({
    host     : 'localhost',
    user     : 'root',
    password : 'ankit@123',
    database : 'pic_short'
});

connection.connect(function(err) {
    if (err) throw err;
});

module.exports = connection;